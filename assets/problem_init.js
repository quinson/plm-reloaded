document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems, {});

    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, {});

    var el = document.querySelectorAll('.tabs');
    var instance = M.Tabs.init(el, {});


    console.log(document.querySelectorAll('.resizer-horizontal, .resizer-vertical'));
    document.querySelectorAll('.resizer-horizontal, .resizer-vertical').forEach(resizer => {
        resizer.addEventListener('mousedown', (e) => {
            e.preventDefault();

            const panel = resizer.previousElementSibling;
            const parent = panel.parentElement;
            const isHorizontal = resizer.classList.contains('resizer-horizontal');

            const onMouseMove = (e) => {
                if (isHorizontal) {
                    const newHeight = (e.clientY - panel.getBoundingClientRect().top) / parent.clientHeight * 100;
                    panel.style.height = `${newHeight}%`;
                    resizer.style.top = `${newHeight}%`;
                } else {
                    const newWidth = (e.clientX - panel.getBoundingClientRect().left) / parent.clientWidth * 100;
                    resizer.style.left = `${newWidth}%`;
                    panel.style.width = `${newWidth}%`;
                }
            };

            const onMouseUp = () => {
                document.removeEventListener('mousemove', onMouseMove);
                document.removeEventListener('mouseup', onMouseUp);
            };

            document.addEventListener('mousemove', onMouseMove);
            document.addEventListener('mouseup', onMouseUp);
        });
    });
});

