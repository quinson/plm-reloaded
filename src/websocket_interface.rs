use axum::{
    extract::{
        ws::{CloseFrame, Message, WebSocket, WebSocketUpgrade},
        ConnectInfo, TypedHeader,
    },
    headers,
    response::IntoResponse,
};
use futures::{SinkExt, StreamExt};
use std::{borrow::Cow, net::SocketAddr, ops::ControlFlow};

//mod runner;
//use runner::RunnerMessage;

use crate::worlds::{create_world, Params, RunnerMessage};

use axum::extract::Query;

pub async fn websocket_handler(
    Query(params): Query<Params>,
    ws: WebSocketUpgrade,
    user_agent: Option<TypedHeader<headers::UserAgent>>,
    ConnectInfo(addr): ConnectInfo<SocketAddr>,
) -> impl IntoResponse {
    let user_agent = if let Some(TypedHeader(user_agent)) = user_agent {
        user_agent.to_string()
    } else {
        String::from("Unknown browser")
    };
    println!("`{}` at {} connected.", user_agent, addr);
    // finalize the upgrade process by returning upgrade callback.
    // we can customize the callback by sending additional info such as address.
    ws.on_upgrade(move |socket| handle_socket(socket, addr, params))
}

async fn handle_socket(stream: WebSocket, who: SocketAddr, params: Params) {
    // By splitting, we can send and receive at the same time.
    let (mut sender, mut receiver) = stream.split();

    // Create a future mpsc channel to send messages to the receiver task.
    let (tx, mut rx) = tokio::sync::mpsc::channel::<RunnerMessage>(100);

    let mut send_task = tokio::spawn(async move {
        // Wait for messages from the channel and send them to the client.
        while let Some(msg) = rx.recv().await {
            match msg {
                RunnerMessage::Svg(svg) => {
                    // Create serde_json::Value from svg
                    let json = serde_json::json!({
                        "type": "svg",
                        "content": svg,
                    });
                    let msg = Message::Text(json.to_string());
                    if sender.send(msg).await.is_err() {
                        println!("Error while sending message");
                        break;
                    }
                }
                RunnerMessage::Log(log) => {
                    let json = serde_json::json!({
                        "type": "log",
                        "content": log,
                    });
                    let msg = Message::Text(json.to_string());
                    if sender.send(msg).await.is_err() {
                        println!("Error while sending message");
                        break;
                    }
                }
                RunnerMessage::Error(err) => {
                    let json = serde_json::json!({
                        "type": "error",
                        "content": err,
                    });
                    let msg = Message::Text(json.to_string());
                    if sender.send(msg).await.is_err() {
                        println!("Error while sending message");
                        break;
                    }
                }
                RunnerMessage::Control(ctrl) => {
                    let json = serde_json::json!({
                        "type": "control",
                        "content": ctrl,
                    });
                    let msg = Message::Text(json.to_string());
                    if sender.send(msg).await.is_err() {
                        println!("Error while sending message");
                        break;
                    }
                }
            }
        }
        if let Err(e) = sender
            .send(Message::Close(Some(CloseFrame {
                code: axum::extract::ws::close_code::NORMAL,
                reason: Cow::from("Goodbye"),
            })))
            .await
        {
            println!("Could not send Close due to {}, probably it is ok?", e);
        }
        0
    });

    let mut recv_task = tokio::spawn(async move {
        let mut world = create_world(params, tx);
        let mut is_running: bool = false;
        let mut active = true;
        while active {
            // If no runner is active just wait for a message
            if !is_running {
                while let Some(Ok(msg)) = receiver.next().await {
                    match msg {
                        Message::Text(t) => {
                            // FIX ME : Check multiple str encoding
                            let json: serde_json::Value = serde_json::from_str(&t).unwrap();
                            
                            if json["type"] == "start" {
                                is_running = true;
                                let file = json["code"].as_str().unwrap();
                                world.reset().await;
                                world.start(file.to_owned()).await;
                                break;
                            }
                        }
                        Message::Close(c) => {
                            println!(">>> {} sent close with code {}", who, c.unwrap().code);
                            active = false;
                            break;
                        }
                        _ => {
                            println!(">>> {} sent non-text message", who);
                        }
                    }
                }
            } else {
                // If a runner is active, select between receiving messages and receiving messages from the runner
                tokio::select! {
                    //biased;

                    Some(Ok(msg)) = receiver.next() => {
                        match msg {
                            Message::Text(t) => {
                                let json: serde_json::Value = serde_json::from_str(&t).unwrap();
                                if json["type"] == "stop" {
                                    is_running = false;
                                }
                            }
                            Message::Close(c) => {
                                println!(">>> {} sent close with code {}", who, c.unwrap().code);
                                break;
                            }
                            _ => {
                                println!(">>> {} sent non-text message", who);
                            }
                        }
                    },
                    Some(msg) = world.next() => {
                        match msg {
                            ControlFlow::Continue(_) => {},
                            ControlFlow::Break(_) => {
                                is_running = false;
                            }
                        }
                    }
                }
            }
        }
    });

    // If any one of the tasks exit, abort the other.
    tokio::select! {
        rv_a = (&mut send_task) => {
            match rv_a {
                Ok(a) => println!("{} messages sent to {}", a, who),
                Err(a) => println!("Error sending messages {:?}", a)
            }
            recv_task.abort();
        },
        rv_b = (&mut recv_task) => {
            match rv_b {
                Ok(_) => println!("Loop exited"),
                Err(_) => println!("Error receiving messages")
            }
            send_task.abort();
        }
    }
}
