use std::os::fd::FromRawFd;

use async_trait::async_trait;
use tokio::{process::{Command, ChildStdin, ChildStdout, ChildStderr}, io::{AsyncBufReadExt, BufReader, Lines, AsyncWriteExt}, fs::File};
use tokio::sync::mpsc::Sender;

use crate::worlds::RunnerMessage;

pub enum ProcessMessage {
    Ack,
    End,
    Error,
    Command(String)
}

pub struct Worker {
    language: String,
    channel: Sender<RunnerMessage>,
    stdin: Option<ChildStdin>,
    stdout: Option<Lines<BufReader<ChildStdout>>>,
    stderr: Option<Lines<BufReader<ChildStderr>>>,
    prog: Option<Lines<BufReader<File>>>,
    process: Option<tokio::process::Child>,
}

impl Worker {
    pub fn new(language: String, channel: Sender<RunnerMessage>) -> Self {
        Self {
            language,
            channel,
            stdin: None,
            stdout: None,
            stderr: None,
            prog: None,
            process: None,
        }
    }
}

#[async_trait]
pub trait Workable {
    async fn launch(&mut self, file: String);
    async fn get_instruction(&mut self) -> String;
    async fn respond(&mut self, answer: ProcessMessage);
}

#[async_trait]
impl Workable for Worker {
    async fn launch(&mut self, file: String) {
        let (read_prog, write_prog) = nix::unistd::pipe().unwrap();

        // Create a child process with stdin, stdout, stderr and the new pipe as pipe
        unsafe {
            let mut child = Command::new(&self.language)
                .args(&["-u".to_string(), file,  "--fdprog".to_string(), write_prog.to_string()])
                .stdin(std::process::Stdio::piped())
                .stdout(std::process::Stdio::piped())
                .stderr(std::process::Stdio::piped())
                .spawn()
                .expect("failed to spawn child");

            let stdin = child.stdin.take().unwrap();
            let stdout = child.stdout.take().unwrap();
            let stderr = child.stderr.take().unwrap();
            let prog_pipe = tokio::fs::File::from_std(std::fs::File::from_raw_fd(read_prog));
        
            // Create a buffer to read the output of the child process (AsyncBufReadExt)
            let prog_buffer = tokio::io::BufReader::new(prog_pipe).lines();
            let stdout_buffer = tokio::io::BufReader::new(stdout).lines();
            let stderr_buffer = tokio::io::BufReader::new(stderr).lines();

            self.stdin = Some(stdin);
            self.stdout = Some(stdout_buffer);
            self.stderr = Some(stderr_buffer);
            self.prog = Some(prog_buffer);
            self.process = Some(child);
        }
    }

    async fn get_instruction(&mut self) -> String {
        let instruction;
        let stdout_buffer = self.stdout.as_mut().unwrap();
        let mut stdout_present = true;
        let stderr_buffer = self.stderr.as_mut().unwrap();
        let mut stderr_present = true;
        let prog_buffer = self.prog.as_mut().unwrap();

        loop {
            tokio::select! {
                biased;
                line = stderr_buffer.next_line(), if stderr_present => {
                    match line {
                        Ok(line) => {
                            let line = match line {
                                Some(line) => line,
                                None => {stderr_present = false;"".to_string()},
                            };
                            // Send a message to the channel
                            if stderr_present {
                                self.channel.send(RunnerMessage::Error(line)).await.unwrap();
                            }
                        },
                        Err(_) => {
                            stderr_present = false;
                        }
                    }
                }

                line = stdout_buffer.next_line(), if stdout_present => {
                    match line {
                        Ok(line) => {
                            let line = match line {
                                Some(line) => line,
                                None => {stdout_present = false;"".to_string()},
                            };
                            // Send a message to the channel
                            if stdout_present {
                                self.channel.send(RunnerMessage::Log(line)).await.unwrap();
                            }
                        },
                        Err(_) => {
                            stdout_present = false;
                        },
                    }
                }
                
                line = prog_buffer.next_line() => {
                    let line = line.unwrap().unwrap();
                    if !line.is_empty() {
                        instruction = line;
                        break;
                    }
                }
            }
        }
        instruction
    }

    async fn respond(&mut self, answer: ProcessMessage) {
        let result = match answer {
            ProcessMessage::Ack => {
                "ACK\n".to_string()
            },
            ProcessMessage::End => {
                "END\n".to_string()
            },
            ProcessMessage::Error => {
                "ERROR\n".to_string()
            },
            ProcessMessage::Command(command) => {
                command
            }, 
        };
        let mut stdin = self.stdin.take().unwrap();
        stdin.write_all(result.as_bytes()).await.unwrap();
        self.stdin = Some(stdin);
    }
}
