/* Test file */

use crate::worlds::Runnable;
use crate::worlds::RunnerMessage;
use crate::worlds::rustacean::RustaceanWorld;

#[test]
fn test_rustacean_world() {
    let (tx, mut rx) = tokio::sync::mpsc::channel(10);
    let mut world = RustaceanWorld::new("".to_string(), tx);
    let code = r#"def move():
return "up""#.to_string();
    let runtime = tokio::runtime::Runtime::new().unwrap();
    runtime.block_on(async move {
        world.start(code).await;
        let msg = rx.recv().await.unwrap();
        assert_eq!(msg, RunnerMessage::Svg(std::fs::read_to_string("assets/img/rustacean-flat-happy.svg").unwrap()));
    });
}
