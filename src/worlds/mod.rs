
use std::ops::ControlFlow;
use askama::Template;
use comrak::ComrakOptions;
use comrak::markdown_to_html;
use tokio::sync::mpsc::Sender;
use async_trait::async_trait;

// Imports worlds
pub mod buggles;
use buggles::BugglesWorld;
use buggles::BugglesWorldHelper;
pub mod rustacean;
use rustacean::RustaceanWorld;

use serde::Deserialize;

pub enum ProgrammingLanguage {
    Python,
    C,
    Java
}

#[derive(Deserialize)]
pub struct Params{
    lang : String, 
    world : String, 
    exercice : String
}

// implement display for this enum
impl std::fmt::Display for ProgrammingLanguage {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            ProgrammingLanguage::Python => write!(f, "Python"),
            ProgrammingLanguage::C => write!(f, "C"),
            ProgrammingLanguage::Java => write!(f, "Java"),
        }
    }
}

// implement from_str for this enum
impl std::str::FromStr for ProgrammingLanguage {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "python" => Ok(ProgrammingLanguage::Python),
            "c" => Ok(ProgrammingLanguage::C),
            "java" => Ok(ProgrammingLanguage::Java),
            _ => Err(()),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum RunnerMessage {
    Svg(String),
    Log(String),
    Error(String),
    Control(String)
}

#[async_trait]
pub trait Runnable : Send {
    // Create a new runner
    async fn start(&mut self, file: String);
    async fn reset(&mut self);
    // Advance a step in the world
    async fn next(&mut self) -> Option<ControlFlow<i32>>;
}

pub fn create_world(params: Params, channel: Sender<RunnerMessage>) -> Box<dyn Runnable> {
    // TODO: Create a world from the language and the world name and init with exercice
    println!("Creating world: {} for language: {}", params.world, params.lang);
    match params.world.as_str() {
        "buggles" => Box::new(BugglesWorld::new(params.exercice, channel)),
        _ => Box::new(RustaceanWorld::new(params.exercice, channel))
    }
}

pub fn create_helper(lang: ProgrammingLanguage, world: String) -> String {
    // Template the helper
    let content = match world.as_str() {
        "buggles" => BugglesWorldHelper { lang: lang.to_string() }.render().unwrap(),
        _ => "".to_string()
    };
    // Convert markdown to html
    let mut options = ComrakOptions::default();
    options.extension.table = true;
    options.extension.strikethrough = true;
    markdown_to_html(&content, &options)
}