use svg::node::element::path::Data;
use svg::node::element::Path;
use svg::Document;

use super::{BugglesWorld, Direction, GridElement};

const BUGGLE_UP: [[i32; 11]; 11] = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
    [0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
    [0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1],
    [0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
];

const BUGGLE_RIGHT: [[i32; 11]; 11] = [
    [0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
    [0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0],
    [0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0],
    [0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0],
    [0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0],
    [0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0],
    [0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0],
    [0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
    [0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0],
];

const BUGGLE_DOWN: [[i32; 11]; 11] = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0],
    [1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1],
    [1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0],
    [0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
];

const BUGGLE_LEFT: [[i32; 11]; 11] = [
    [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0],
    [0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0],
    [0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0],
    [0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0],
    [0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0],
    [0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0],
    [0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0],
    [0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
];

pub fn square(start: (i32, i32), size: i32) -> Data {
    Data::new()
        .move_to(start)
        .line_by((0, size))
        .line_by((size, 0))
        .line_by((0, -size))
        .close()
}

pub trait Drawable {
    fn draw(&self) -> String;
}

impl Drawable for BugglesWorld {
    fn draw(&self) -> String {
        //let data = square((0, 0), 50);

        let mut document =
            Document::new().set("viewBox", (0, 0, 11 * self.width, 11 * self.height));

        for i in 0..self.width {
            for j in 0..self.height {
                let value: String;
                if self.board[i][j].content == GridElement::Bagel {
                    value = "#FF0000".to_string();
                }
                else if (i + j) % 2 == 0 {
                    value = "#808080".to_string();
                } else {
                    value = "#A9A9A9".to_string();
                }
                let path = Path::new()
                    .set("fill", value)
                    .set("d", square(((i * 11).try_into().unwrap(), (1 + (self.height - 1 - j) * 11).try_into().unwrap()), 11));
                document = document.clone().add(path);
                // Check if a buggle in buggles is at the current position
                for buggle in &self.buggles {
                    if buggle.x as usize == i && buggle.y as usize == j {
                        let buggle_scheme = match buggle.direction {
                            Direction::Up => BUGGLE_UP,
                            Direction::Down => BUGGLE_DOWN,
                            Direction::Left => BUGGLE_LEFT,
                            Direction::Right => BUGGLE_RIGHT
                        };
                        for k in 0..buggle_scheme.len() {
                            for l in 0..buggle_scheme.len() {
                                if buggle_scheme[k][l] == 1
                                {
                                    let path = Path::new().set("fill", "black").set(
                                        "d",
                                        square(
                                            (
                                                (i * 11 + l).try_into().unwrap(),
                                                ((self.height - 1 - j) * 11 + 1 + k).try_into().unwrap(),
                                            ),
                                            1,
                                        ),
                                    );
                                    document = document.clone().add(path);
                                }
                            }
                        }
                    }
                }
            }
        }

        let mut vector: Vec<u8> = Vec::new();
        svg::write(&mut vector, &document).expect("Error while writing svg");
        String::from_utf8(vector).unwrap()
    }
}
