import argparse
import os
import sys



def handle_message():
    message = sys.stdin.readline().strip()
    if message in ["FWALL"]:
        return True
    elif message == "ACK":
        pass
    elif message == "END":
        pass
    elif message == "ERROR":
        sys.exit(1)
    else:
        print("Unknown message: {}".format(message))
    return False

def forward():
    global fpd
    print("UP", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    handle_message()

def left():
    global fpd
    print("LEFT", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    handle_message()

def right():
    global fpd
    print("RIGHT", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    handle_message()

def backward():
    global fpd
    print("DOWN", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    handle_message()

def step(x : int):
    for i in range(x):
        forward()

def isFrontFacingWall():
    global fpd
    print("FWALL?", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    return handle_message()

def isBackwardFacingWall():
    global fpd
    print("BWALL?", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    return handle_message()

def isOnBagel():
    global fpd
    print("BWALL?", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    return handle_message()

def brushDown():
    global fpd
    print("BRUSH_DOWN", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    handle_message()

def brushUp():
    global fpd
    print("BRUSH_UP", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    handle_message()

def isBrushDown():
    global fpd
    print("BRUSH?", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    return handle_message()

def setBrushColor(color : str):
    global fpd
    print("BRUSH_COLOR {}".format(color), file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    handle_message()

def getBrushColor():
    global fpd
    print("BRUSH_COLOR?", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    return handle_message()

def getHeading():
    global fpd
    print("HEADING?", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    return handle_message()

def setHeading(heading : str):
    global fpd
    print("HEADING {}".format(heading), file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    handle_message()


def end():
    global fpd
    print("END", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    handle_message()

def main():
    {{code}}
    pass

if '__main__' in __name__:
    # parse the the command line arguments and store them in args
    parser = argparse.ArgumentParser()
    parser.add_argument('--fdprog', type=int)
    args = parser.parse_args()
    # Open file descriptors
    fpd = os.fdopen(args.fdprog, 'w')
    # pritn to stderr
    main()
    end()

    