use std::ops::ControlFlow;

use askama::Template;
use async_trait::async_trait;
use axum::extract::rejection::FailedToDeserializeQueryString;
use tokio::sync::mpsc::Sender;

use super::ProgrammingLanguage;
use super::{Runnable, RunnerMessage};

mod drawer;
use drawer::Drawable;

use crate::file_formatter::write_file;

use super::super::worker::Worker;
use crate::worker::Workable;

#[derive(Template)]
#[template(path = "worlds/buggles/helper.md")]
pub struct BugglesWorldHelper {
    pub lang: String,
}

#[derive(Hash, Eq, PartialEq)]

enum Direction {
    Up,
    Down,
    Left,
    Right,
}

enum Question {
    Fwall,
    Bwall,
    Brush,
    Heading,
}

enum Instr {
    Dir(Direction),
    Quest(Question),
}

#[derive(PartialEq)]
enum GridElement {
    Void,
    Bagel,
}

enum Color {
    None,
    Red,
    Blue
}

struct Cell {
    content: GridElement,
    color: Color,
    top_wall: bool,
    left_wall: bool,
}


struct Buggle {
    direction: Direction,
    x: i8,
    y: i8,
}

pub struct BugglesWorld {
    exercice: String,
    channel: Sender<RunnerMessage>,
    worker: Worker,
    board: Vec<Vec<Cell>>,
    buggles: Vec<Buggle>,
    width: usize,
    height: usize,
}

fn init(exercice: &str, board: &mut Vec<Vec<Cell>>, buggles: &mut Vec<Buggle>) {
    println!("Loading default {}", exercice);
    for i in 0..board.capacity() {
        board.push(Vec::with_capacity(board.capacity()));
        for _ in 0..board.capacity() {
            board[i].push(Cell {
                content: GridElement::Void,
                color: Color::None,
                top_wall: false,
                left_wall: false,
            });
        }
    }
    board[0][0] = Cell{content:GridElement::Bagel, color: Color::Red, top_wall:false, left_wall:false};
    board[4][4] = Cell{content:GridElement::Bagel, color: Color::Blue, top_wall:false, left_wall:false};
    let buggle = Buggle {
        direction: Direction::Up,
        x: 2,
        y: 2,
    };
    buggles.push(buggle);
}

impl BugglesWorld {
    pub fn new(exercice: String, channel: Sender<RunnerMessage>) -> Self {
        let mut board = Vec::with_capacity(5);
        let mut buggles = Vec::new();
        init(&exercice, &mut board, &mut buggles);
        Self {
            exercice,
            channel: channel.clone(),
            worker: Worker::new("python3".to_string(), channel),
            board,
            buggles,
            width: 5,
            height: 5,
        }
    }

    pub async fn send_current_state(&mut self) {
        self.channel
            .send(RunnerMessage::Svg(self.draw()))
            .await
            .unwrap();
    }

    pub async fn send_error_message(&mut self, line: String) {
        self.channel.send(RunnerMessage::Error(line)).await.unwrap();
    }

    async fn manage_question(&mut self, index: usize, question: Question) {
        match question {
            Question::Fwall => {
                self.worker.respond(crate::worker::ProcessMessage::Command("FWALL\n".to_string())).await;
            },
            _ => {
                self.worker
                .respond(crate::worker::ProcessMessage::Ack)
                .await;
            },
        }
    }

    async fn manage_move(&mut self, index: usize, direction: Direction) -> bool {
        let mut front_x = self.buggles[index].x;
        let mut front_y = self.buggles[index].y;
        let mut back_x = self.buggles[index].x;
        let mut back_y = self.buggles[index].y;
        match self.buggles[index].direction {
            Direction::Up => {
                front_y += 1;
                back_y -= 1;
            }
            Direction::Down => {
                front_y -= 1;
                back_y += 1;
            }
            Direction::Left => {
                front_x -= 1;
                back_x += 1;
            }
            Direction::Right => {
                front_x += 1;
                back_x -= 1;
            }
        }
        match direction {
            Direction::Up => {
                if 0 > front_x
                    || front_x >= self.width.try_into().unwrap()
                    || 0 > front_y
                    || front_y >= self.height.try_into().unwrap()
                {
                    self.send_error_message("Un buggle est sorti par le haut!".to_string())
                        .await;
                    return false; 
                }
                /* TODO corriger mur
                else if self.board[self.buggles[index].x as usize][self.buggles[index].y as usize].top_wall{
                    self.send_error_message("Un buggle est tombé sur un rocher!".to_string())
                        .await;
                    return false;
                } */
                else {
                    self.buggles[index].x = front_x;
                    self.buggles[index].y = front_y;
                }
            }
            Direction::Down => {
                if 0 > back_x
                    || back_x >= self.width.try_into().unwrap()
                    || 0 > back_y
                    || back_y >= self.height.try_into().unwrap()
                {
                    self.send_error_message("Un buggle est sorti par le bas!".to_string())
                        .await;
                    return false;
                } else {
                    self.buggles[index].x = back_x;
                    self.buggles[index].y = back_y;
                }
            }
            Direction::Left => match self.buggles[index].direction {
                Direction::Up => {
                    self.buggles[index].direction = Direction::Left;
                }
                Direction::Down => {
                    self.buggles[index].direction = Direction::Right;
                }
                Direction::Left => {
                    self.buggles[index].direction = Direction::Down;
                }
                Direction::Right => {
                    self.buggles[index].direction = Direction::Up;
                }
            },
            Direction::Right => match self.buggles[index].direction {
                Direction::Up => {
                    self.buggles[index].direction = Direction::Right;
                }
                Direction::Down => {
                    self.buggles[index].direction = Direction::Left;
                }
                Direction::Left => {
                    self.buggles[index].direction = Direction::Up;
                }
                Direction::Right => {
                    self.buggles[index].direction = Direction::Down;
                }
            },
        }
        true
    }
}

#[async_trait]
impl Runnable for BugglesWorld {
    async fn start(&mut self, code: String) {
        // Regex trick to unescape the " character
        let path = write_file(
            ProgrammingLanguage::Python,
            "src/worlds/buggles/support.py".to_string(),
            code,
        );
        self.worker.launch(path.to_string()).await;
        self.send_current_state().await;
    }

    async fn reset(&mut self) {
        // Empty self.board and self.buggles
        Vec::clear(&mut self.board);
        Vec::clear(&mut self.buggles);
        init(&self.exercice, &mut self.board, &mut self.buggles)
        // TODO : self.worker.kill();
    }

    async fn next(&mut self) -> Option<ControlFlow<i32>> {
        // Read the stdprog of the process
        let instruction = self.worker.get_instruction().await;
        if instruction == "END" {
            self.worker
                .respond(crate::worker::ProcessMessage::End)
                .await;
            self.channel
                .send(RunnerMessage::Control("end".to_string()))
                .await
                .unwrap();
            return Some(ControlFlow::Break(0));
        }

        // tokio sleep to wait for the process to write the instruction
        tokio::time::sleep(std::time::Duration::from_millis(100)).await;
        // Parse the line to a Move (Up,Down)
        let move_instr = match instruction.as_str() {
            "UP" => Some(Instr::Dir(Direction::Up)),
            "DOWN" => Some(Instr::Dir(Direction::Down)),
            "LEFT" => Some(Instr::Dir(Direction::Left)),
            "RIGHT" => Some(Instr::Dir(Direction::Right)),
            "FWALL?" => Some(Instr::Quest(Question::Fwall)),
            _ => None,
        };

        // if the move is not the same as the internal state,
        // change the internal state and send a message to the channel
        if let Some(instr) = move_instr {
            if let Instr::Quest(question) = instr {
                self.manage_question(0, question).await;
            } else {
                if let Instr::Dir(direction) = instr {
                    if self.manage_move(0, direction).await {
                        self.worker
                            .respond(crate::worker::ProcessMessage::Ack)
                            .await;
                
                    } else {
                        self.worker
                            .respond(crate::worker::ProcessMessage::Error)
                            .await;
                        self.channel
                            .send(RunnerMessage::Control("end".to_string()))
                            .await
                            .unwrap();
                        return Some(ControlFlow::Break(0));
                    }
                    self.send_current_state().await;
                }
            }
        }
        Some(ControlFlow::Continue(()))
    }
}
