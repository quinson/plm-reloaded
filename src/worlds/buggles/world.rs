


pub struct World {
    pub color: String,
}

pub trait ImplWorld {
    fn new(color: String) -> Self;

    fn update(&mut self, instructions: String);
}

impl ImplWorld for World {
    fn new(color: String) -> Self {
        Self { color }
    }

    fn update(&mut self, instructions: String) {
        self.color = instructions;
    }
}