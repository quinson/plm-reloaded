{%- import "macro.html" as scope -%}
# BuggleWorld

This world was invented by Lyn Turbak, at Wellesley College. It is full of
Buggles, little animals understanding simple orders, and offers numerous
possibilities of interaction with the world: taking or dropping objects,
paint the ground, hit walls, etc.

## Methods understood by buggles

### Moving

| Description | Command                                                                                      |
| ----------- | -------------------------------------------------------------------------------------------- |
| Turn left   | `{% call scope::typing(lang, "left", "void") %}()`                                           |
| Turn right  | `{% call scope::typing(lang, "right", "void") %}()`                                          |
| Forward     | `{% call scope::typing(lang, "forward", "void") %}()`                                        |
| Backward    | `{% call scope::typing(lang, "backward", "void") %}()`                                       |
| Step        | `{% call scope::typing(lang, "step", "void") %}({% call scope::typing(lang, "x", "int") %})` |



### Information on the buggle

| Description              | Command                                                                                                    |
| ------------------------ | ---------------------------------------------------------------------------------------------------------- |
| Get Color                | `{% call scope::typing(lang, "getColor", "Color") %}()`                                                    |
| Set Color                | `{% call scope::typing(lang, "setColor", "void") %}({% call scope::typing(lang, "color", "Color") %} )`    |
| Look for a wall forward  | `{% call scope::typing(lang, "isFacingWall", "boolean") %}()`                                              |
| Look for a wall backward | `{% call scope::typing(lang, "isBackwardFacingWall", "boolean") %}()`                                      |
| Get heading              | `{% call scope::typing(lang, "getHeading", "Direction") %}()`                                              |
| Set heading              | `{% call scope::typing(lang, "setHeading", "void") %}({% call scope::typing(lang, "dir", "Direction") %})` |

### About the brush

| Description        | Command                                                                                                     |
| ------------------ | ----------------------------------------------------------------------------------------------------------- |
| Brush down         | `{% call scope::typing(lang, "brushDown", "void") %}()`                                                     |
| Brush up           | `{% call scope::typing(lang, "brushUp", "void") %}()`                                                       |
| Is brush down?     | `{% call scope::typing(lang, "isBrushDown", "boolean") %}()`                                                |
| Change brush color | `{% call scope::typing(lang, "setBrushColor", "void") %}({% call scope::typing(lang, "color", "Color") %})` |
| Get brush color    | `{% call scope::typing(lang, "getBrushColor", "Color") %}()`                                                |

### Interacting with the world

## Note on exceptions
Regular buggles throw a BuggleWallException exception if you ask them to
traverse a wall.  They throw a NoBaggleUnderBuggleException exception if you
ask them to pickup a baggle from an empty cell, or a
AlreadyHaveBaggleException exception if they already carry a baggle.  Trying
to drop a baggle on a cell already containing one throws an
AlreadyHaveBaggleException exception. 
Dropping a baggle when you have none throws a DontHaveBaggleException.

SimpleBuggles (ie, the one used in first exercises) display an error message
on problem so that you don't need to know what an exception is.
