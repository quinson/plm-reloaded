use std::ops::ControlFlow;

use tokio::sync::mpsc::Sender;

use async_trait::async_trait;

use crate::worker::Workable;

use super::super::worker::Worker;
use super::{ProgrammingLanguage, Runnable, RunnerMessage};

use super::super::file_formatter::write_file;

#[derive(PartialEq, Clone, Copy)]
enum Move {
    Up,
    Down,
}

pub struct RustaceanWorld {
    exercice: String,
    channel: Sender<RunnerMessage>,
    internal_state: Move,
    worker: Worker,
}

impl RustaceanWorld {
    pub fn new(exercice: String, channel: Sender<RunnerMessage>) -> Self {
        Self {
            exercice,
            channel: channel.clone(),
            internal_state: Move::Up,
            worker: Worker::new("python3".to_string(), channel),
        }
    }
    async fn send_current_state(&mut self) {
        match self.internal_state {
            Move::Up => {
                self.channel
                    .send(RunnerMessage::Svg(
                        std::fs::read_to_string("assets/img/rustacean-flat-happy.svg").unwrap(),
                    ))
                    .await
                    .unwrap();
            }
            Move::Down => {
                self.channel
                    .send(RunnerMessage::Svg(
                        std::fs::read_to_string("assets/img/rustacean-flat-gesture.svg").unwrap(),
                    ))
                    .await
                    .unwrap();
            }
        }
    }
}

#[async_trait]
impl Runnable for RustaceanWorld {
    async fn start(&mut self, code: String) {
        // Regex trick to unescape the " character
        let path = write_file(
            ProgrammingLanguage::Python,
            "src/worlds/rustacean/support.py".to_string(),
            code,
        );

        // Send initial state and create the process
        self.send_current_state().await;
        self.worker.launch(path.to_string()).await;
    }

    async fn reset(&mut self) {
        println!("{}", self.exercice);
        self.internal_state = Move::Up;
        self.send_current_state().await;
    }

    async fn next(&mut self) -> Option<ControlFlow<i32>> {
        // Read the stdprog of the process
        let instruction = self.worker.get_instruction().await;

        if instruction == "END" {
            self.worker
                .respond(crate::worker::ProcessMessage::End)
                .await;
            self.channel
                .send(RunnerMessage::Control("end".to_string()))
                .await
                .unwrap();
            return Some(ControlFlow::Break(0));
        }
        self.worker
            .respond(crate::worker::ProcessMessage::Ack)
            .await;
        // tokio sleep
        tokio::time::sleep(std::time::Duration::from_millis(1000)).await;
        // Parse the line to a Move (Up,Down)
        let move_instr = match instruction.as_str() {
            "UP" => Some(Move::Up),
            "DOWN" => Some(Move::Down),
            _ => None,
        };

        // if the move is not the same as the internal state,
        // change the internal state and send a message to the channel
        if let Some(r#move) = move_instr {
            if r#move != self.internal_state {
                self.internal_state = r#move;
                self.send_current_state().await;
            }
        }

        Some(ControlFlow::Continue(()))
    }
}
