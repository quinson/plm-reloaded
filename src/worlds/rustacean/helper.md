# Rustacean World

Rustacean World is a world where you can do nothing.

## Available commands

| Command     | Description |
| ----------- | ----------- |
| `rustacean` | Do nothing  |

