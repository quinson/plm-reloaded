import argparse
import os
import sys

def handle_message(message):
    if message == "ACK":
        pass
    elif message == "END":
        pass
    elif message == "ERROR":
        sys.exit(1)
    else:
        print("Unknown message: {}".format(message))

def up():
    global fpd
    print("UP", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    handle_message(sys.stdin.readline().strip())

def down():
    global fpd
    print("DOWN", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()
    # Wait for the Ok in stdin
    handle_message(sys.stdin.readline().strip())

def end():
    global fpd
    print("END", file=fpd)
    # flush the buffer to make sure the message is sent
    fpd.flush()

def main():
    {{code}}
    pass

if '__main__' in __name__:
    # parse the the command line arguments and store them in args
    parser = argparse.ArgumentParser()
    parser.add_argument('--fdprog', type=int)
    args = parser.parse_args()
    # Open file descriptors
    fpd = os.fdopen(args.fdprog, 'w')
    # pritn to stderr
    print("test1")
    main()
    print("test2")
    end()
    print("test3")
    