use std::str::FromStr;

use askama::Template;
use axum::{
    extract,
    response::{Html, IntoResponse, Response}, http::StatusCode,
};

use crate::worlds::{create_helper, ProgrammingLanguage};

// Include utf-8 file at **compile** time.
pub async fn index() -> Html<&'static str> {
    Html(std::include_str!("../templates/index.html"))
}

pub async fn problem(extract::Path((lang, world, exercice)): extract::Path<(String, String, String)>) -> impl IntoResponse {
    let world_helper = create_helper(ProgrammingLanguage::from_str(&lang).unwrap(), world.clone());
    let template = ProblemTemplate { lang, world, exercice, world_helper };
    HtmlTemplate(template)
}

#[derive(Template)]
#[template(path = "problem.html")]
struct ProblemTemplate {
    lang : String, 
    world : String, 
    exercice : String,
    world_helper : String,
}

struct HtmlTemplate<T>(T);

impl<T> IntoResponse for HtmlTemplate<T>
where
    T: Template,
{
    fn into_response(self) -> Response {
        match self.0.render() {
            Ok(html) => Html(html).into_response(),
            Err(err) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Failed to render template. Error: {}", err),
            )
                .into_response(),
        }
    }
}