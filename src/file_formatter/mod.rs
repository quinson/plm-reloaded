use rand::distributions::{Alphanumeric, DistString};

use crate::worlds::ProgrammingLanguage;

use std::fs;



// function that from a string write it in a file and return the path of the file
pub fn write_file(lang : ProgrammingLanguage, support : String, content: String) -> String {
    //Generate random letters name for the file
    let random = Alphanumeric.sample_string(&mut rand::thread_rng(), 16);
    // add 4 spaces to the beginning of each line (except the first one)
    let lines = content.split("\n");
    let content = lines
        .enumerate()
        .map(|(i, line)| {
            if i == 0 {
                line.to_string()
            } else {
                format!("    {}", line)
            }
        })
        .collect::<Vec<String>>()
        .join("\n");

    // Open the tempalte file
    let template = std::fs::read_to_string(support).unwrap();
    // Replace {{code}} with the code of the file without the first and last character
    let template = template.replace("{{code}}", &content);

    // Save the file in tmp/tmp.py
    // TODO : This processing is specififc to python, it should be done in the final template
    match lang {
        ProgrammingLanguage::Python => {
            fs::create_dir("tmp").unwrap_or_default();
            let final_path = format!("tmp/{}.py", random);
            std::fs::write(final_path.clone(), template).unwrap();
            final_path
        }
        _ => {
            //panic
            String::from("")
        }
    }
}